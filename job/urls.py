from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin

from content.views import FullSearchView

urlpatterns = list()

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'^', include('job_catalog.urls', namespace='catalog')),
    url(r'^', include('content.urls', namespace='content')),
    url(r'^admin/', admin.site.urls),

    url(r'^search/', FullSearchView.as_view(), name='search'),
    url(r'^accounts/', include('users.accounts_urls')),
    url(r'^profile/', include('users.urls', namespace='profile')),
    url(r'^banner/', include('banner.urls', namespace='banner')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^polls/', include('poll.urls', namespace='poll')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]
