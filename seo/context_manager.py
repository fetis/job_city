from django.conf import settings

from seo.models import MetaTag


def meta_title_and_description(request):
    data = {'META_TITLE': '', 'META_DESCRIPTION': ''}
    meta = MetaTag.objects.filter(path=request.path).first()
    if meta:
        data['META_TITLE'] = "{}{}".format(settings.PREFIX_META_TITLE, meta.meta_title)
        data['META_DESCRIPTION'] = meta.meta_description
    else:
        data['META_TITLE'] = settings.PREFIX_META_TITLE
    return data
