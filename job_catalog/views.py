from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import CreateView, ListView, UpdateView, View, TemplateView, FormView
from pure_pagination import PaginationMixin

from registration.backends.hmac.views import RegistrationView as HmacRegistrationView

from job.utils import json_response, MenuMixin
from job_catalog.forms import CVModelAndRegistrationForm, VacancyAndRegistrationForm, CVModelForm, VacancyModelForm, \
    ReplyCVForm, VacancyRequestForm
from job_catalog.models import CV, Vacancy
from users.helpers import OnlyEmployerMixin, OnlyApplicantMixin
from users.models import ApplicantProfile, EmployerProfile


class IndexView(MenuMixin, PaginationMixin, ListView):
    template_name = 'index.html'
    paginate_by = 10
    ad_slug = None
    menu_slug = 'index'
    is_vacancy = False

    def get(self, request, *args, **kwargs):
        if 'view' in request.GET:
            self.is_vacancy = self.request.GET.get('view') == 'v'
        elif 'qc' in request.GET or 'qv' in request.GET:
            if request.GET.get('qv'):
                self.is_vacancy = True
        else:
            if self.request.user.is_authenticated and self.request.user.profile_applicant:
                self.is_vacancy = True
        return super().get(request, *args, **kwargs)

    def get_context_object_name(self, object_list):
        if self.is_vacancy:
            return 'vacancy_list'
        else:
            return 'cv_list'

    def get_queryset(self):
        if self.is_vacancy:
            qs = Vacancy.objects.get_vacancy_for_listing()
        else:
            qs = CV.objects.get_cv_for_listing()

        if self.request.GET.get('vacancy') and self.request.GET['vacancy'].isdigit():
            return qs.filter(pk=self.request.GET['vacancy'])

        if self.request.GET.get('qv'):
            qs = qs.filter(title__contains=self.request.GET['qv'])
        if self.request.GET.get('qc'):
            qs = qs.filter(position__contains=self.request.GET['qc'])
        if self.request.GET.get('cat'):
            pk = self.request.GET['cat'] if self.request.GET['cat'].isdigit() else 0
            qs = qs.filter(category__pk=pk)
        qs = qs.order_by('-in_top')
        return qs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['is_vacancy'] = self.is_vacancy
        return ctx


class CreateAdvertRegistrationView(HmacRegistrationView):
    template_name = 'create_cv.html'
    model = CV

    def register(self, form):
        user = super().register(form)
        data = form.cleaned_data
        if isinstance(form, CVModelAndRegistrationForm):
            profile = ApplicantProfile(phone=data.get('phone'), first_name=data.get('first_name'),
                                       last_name=data.get('last_name'), third_name=data.get('third_name'),
                                       birth_day=data.get('birth_day'), user=user)
        elif isinstance(form, VacancyAndRegistrationForm):
            profile = EmployerProfile(phone=data.get('phone'), company=data.get('company'), contact=data.get('contact'),
                                      address=data.get('address'), user=user)
        else:
            profile = None
        if profile:
            profile.save()
            if isinstance(self.model, CV):
                cv = CV()
                cv.category = data.get('category')
                cv.position = data.get('position')
                cv.desired_salary = data.get('desired_salary')
                cv.education = data.get('education')
                cv.experience = data.get('experience')
                cv.last_work = data.get('last_work')
                cv.profile = profile
                cv.save()
            elif isinstance(self.model, Vacancy):
                vacancy = Vacancy()
                vacancy.category = data.get('category')
                vacancy.title = data.get('title')
                vacancy.experience = data.get('experience')
                vacancy.charge = data.get('charge')
                vacancy.description = data.get('description')
                vacancy.requirements = data.get('requirements')
                vacancy.conditions = data.get('conditions')
                vacancy.salary_min = data.get('salary_min')
                vacancy.salary_max = data.get('salary_max')
                vacancy.profile = profile
                vacancy.save()
        return user


class CreateCVView(MenuMixin, OnlyApplicantMixin, CreateView):
    template_name = 'create_cv.html'
    form_class = CVModelForm
    menu_slug = 'index'

    def get_success_url(self):
        return reverse('catalog:index')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.request.user.profile_applicant.photo:
            ctx['photo'] = self.request.user.profile_applicant.photo
        return ctx


class CreateVacancyView(MenuMixin, CreateView):
    template_name = 'create_vacancy_request.html'
    form_class = VacancyRequestForm
    menu_slug = 'index'

    def get_success_url(self):
        return reverse('catalog:index')

    def send_mail_manager(self):
        subject = 'Создан новый запрос на добавление вакансии'
        message = 'Детально вакансию можно увидеть тут {}'
        vacancy_path = '/admin/job_catalog/vacancyrequest/{}/change/'.format(self.object.pk)
        url = 'http://{}{}'.format(self.request.META['HTTP_HOST'], vacancy_path)
        message = message.format(url)
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [settings.VACANCY_MANAGER_EMAIL])

    def form_valid(self, form):
        self.object = form.save()
        self.send_mail_manager()
        return super().form_valid(form)


class MyCvList(OnlyApplicantMixin, PaginationMixin, ListView):
    template_name = 'my_cv.html'
    paginate_by = 10
    context_object_name = 'cv_list'

    def get_queryset(self):
        return CV.objects.filter(profile=self.request.user.profile_applicant)


class MyVacancyList(OnlyEmployerMixin, PaginationMixin, ListView):
    template_name = 'my_vacancies.html'
    paginate_by = 10
    context_object_name = 'vacancy_list'

    def get_queryset(self):
        return Vacancy.objects.filter(profile=self.request.user.profile_employer)


class VacancyEditView(OnlyEmployerMixin, UpdateView):
    template_name = 'update_vacancy.html'
    form_class = VacancyModelForm

    def form_valid(self, form):
        vacancy = form.save(commit=False)
        vacancy.status = 1
        vacancy.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_queryset(self):
        return Vacancy.objects.filter(profile=self.request.user.profile_employer)

    def get_success_url(self):
        return reverse('catalog:my_vac_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs


class CVEditView(OnlyApplicantMixin, UpdateView):
    template_name = 'update_cv.html'
    form_class = CVModelForm

    def form_valid(self, form):
        cv = form.save(commit=False)
        cv.status = 1
        cv.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_queryset(self):
        return CV.objects.filter(profile=self.request.user.profile_applicant)

    def get_success_url(self):
        return reverse('catalog:my_cv_list')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs


class GetCvInfo(View):

    def get(self, request, *args, **kwargs):
        data = dict()
        if kwargs.get('pk') and kwargs['pk'].isdigit():
            cv = get_object_or_404(CV, pk=kwargs['pk'])
            cv.view_count += 1
            cv.save(update_fields=['view_count'])
            data['edu'] = cv.education
            data['skills'] = cv.skills
            data['last_work'] = cv.last_work
            data['fio'] = "{} {} {}".format(cv.profile.last_name, cv.profile.first_name, cv.profile.third_name)
            data['birth_date'] = cv.profile.birth_day.strftime('%d.%m.%Y')
            data['email'] = cv.profile.user.email
            data['phone'] = cv.profile.phone
            data['pk'] = cv.pk
            return render(request, '_cv_info.html', data)


class GetVacancyInfo(View):

    def get(self, request, *args, **kwargs):
        data = dict()
        if kwargs.get('pk') and kwargs['pk'].isdigit():
            vacancy = get_object_or_404(Vacancy, pk=kwargs['pk'])
            vacancy.view_count += 1
            vacancy.save(update_fields=['view_count'])
            data['experience'] = vacancy.experience
            data['charge'] = vacancy.charge
            data['description'] = vacancy.description
            data['requirements'] = vacancy.requirements
            data['conditions'] = vacancy.conditions
            data['email'] = vacancy.profile.user.email
            data['phone'] = vacancy.profile.phone
            data['company'] = vacancy.profile.company
            data['pk'] = vacancy.pk
        return render(request, '_vacancy_info.html', data)


class ReplyVacancy(MenuMixin, OnlyApplicantMixin, TemplateView):
    template_name = 'reply_vacancy.html'
    menu_slug = 'index'

    def get(self, request, *args, **kwargs):
        self.cv_list = CV.objects.get_active_cv().filter(profile=self.request.user.profile_applicant)
        if self.cv_list.exists():
            return super().get(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse('category:cv_create'))

    def post(self, request, *args, **kwargs):
        pk = request.POST.get('cv')
        vacancy = get_object_or_404(Vacancy.objects.get_active_vacancies(), pk=self.kwargs['pk'])
        if pk and pk.isdigit():
            cv = get_object_or_404(CV.objects.get_active_cv()
                                   .filter(profile=self.request.user.applicant_profile), pk=pk)
            message_data = {
                'name': "{} {} {}".format(cv.profile.last_name, cv.profile.first_name, cv.profile.third_name),
                'vacancy_name': vacancy.title,
                'position': cv.position,
                'salary': cv.desired_salary,
                'edu': cv.education,
                'exp': cv.experience,
                'last_work': cv.last_work,
                'skill': cv.skills,
                'phone': cv.profile.phone,
                'email': cv.profile.user.email
            }
            subject = 'Отклик на вакансию'
            message = '{name} откликнулся на вакансию "{vacancy_name}". Резюме:\n' \
                      'Предполагаемая должность: {position}\n' \
                      'Желаемая зарплата: {salary} р.\n' \
                      'Образование: {edu}\n' \
                      'Опыт работы: {exp}\n' \
                      'Последнее место работы: {last_work}\n' \
                      'Дополнительные навыки: {skill}\n' \
                      'Контакты: {phone} ({email})'.format(**message_data)
            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [vacancy.profile.user.email])
            return HttpResponseRedirect(reverse('catalog:success_reply'))
        else:
            return HttpResponseRedirect(reverse('catalog:vac_reply', kwargs={'pk': self.kwargs['pk']}))

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['cv_list'] = self.cv_list
        return ctx


class ReplyCv(MenuMixin, OnlyEmployerMixin, FormView):
    template_name = 'reply_cv_form.html'
    form_class = ReplyCVForm
    menu_slug = 'index'

    def send_invitation(self, profile, text, cv):
        subject = "Приглашение на собеседование"
        message = "Вас пригласили на собеседование в компанию {}, сообщение от работодателя: {}" \
            .format(profile.company, text)
        email = cv.profile.user.email
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email], fail_silently=True)

    def form_valid(self, form):
        text = form.cleaned_data.get('text')
        cv = get_object_or_404(CV.objects.get_active_cv(), pk=self.kwargs['pk'])
        self.send_invitation(self.request.user.employer_profile, text, cv)
        return HttpResponseRedirect("{}?s=cv".format(reverse('catalog:success_reply')))


class ReplySuccess(TemplateView):
    template_name = 'sucess_reply.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.request.GET.get('s', None) == 'cv':
            ctx['text'] = 'Приглашение успешно отправлено, <a href="{}">на главную</a>'.format(reverse("catalog:index"))
        else:
            ctx['text'] = 'Отклик успешно отправлен, <a href="{}">на главную</a>'.format(reverse("catalog:index"))
        return ctx


class AddToFavorites(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        pk = kwargs.get('pk')

        if slug and pk and pk.isdigit():
            if slug == 'cv':
                cv = get_object_or_404(CV.objects.get_active_cv(), pk=pk)
                if not request.user.favorites_cv.filter(pk=cv.pk).exists():
                    request.user.favorites_cv.add(cv)
            elif slug == 'vac':
                vacancy = get_object_or_404(Vacancy.objects.get_active_vacancies(), pk=pk)
                if not request.user.favorites_vacancies.filter(pk=vacancy.pk).exists():
                    request.user.favorites_vacancies.add(vacancy)
            favorites_count = request.user.set_favorite_count()
            request.user.set_favorites_ids()
            return json_response({'count': favorites_count, 'url': reverse('catalog:delete_fav', kwargs={'slug': slug,
                                                                                                         'pk': pk})})
        return HttpResponse(status=404)


class DeleteFromFavorites(LoginRequiredMixin, View):

    def remove_fav(self, request, slug, pk):
        if slug == 'cv':
            cv = get_object_or_404(CV.objects.get_active_cv(), pk=pk)
            if request.user.favorites_cv.filter(pk=cv.pk).exists():
                request.user.favorites_cv.remove(cv)
        elif slug == 'vac':
            vacancy = get_object_or_404(Vacancy.objects.get_active_vacancies(), pk=pk)
            if request.user.favorites_vacancies.filter(pk=vacancy.pk).exists():
                request.user.favorites_vacancies.remove(vacancy)

    def get(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        pk = kwargs.get('pk')

        if slug and pk and pk.isdigit():
            self.remove_fav(request, slug, pk)
            request.user.set_favorites_ids()
            request.user.set_favorite_count()
        return HttpResponseRedirect(reverse('catalog:my_fav'))

    def post(self, request, *args, **kwargs):
        slug = kwargs.get('slug')
        pk = kwargs.get('pk')

        if slug and pk and pk.isdigit():
            self.remove_fav(request, slug, pk)
            favorites_count = request.user.set_favorite_count()
            request.user.set_favorites_ids()
            return json_response({'count': favorites_count})
        return HttpResponse(status=404)


class MyFavorites(LoginRequiredMixin, TemplateView):
    template_name = 'my_favorites.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['cv_fav'] = self.request.user.get_favorite_cv()
        ctx['vac_fav'] = self.request.user.get_favorite_vacancies()
        return ctx


class ToPrint(TemplateView):
    template_name = 'cv_print.html'

    def get_object(self):
        pk = self.kwargs.get('pk')
        slug = self.kwargs.get('slug')
        if pk and pk.isdigit() and slug:
            try:
                if slug == 'cv':
                    obj = CV.objects.get_active_cv().get(pk=pk)
                else:
                    obj = Vacancy.objects.get_active_vacancies().get(pk=pk)
                    self.template_name = 'vacancy_print.html'
            except (CV.DoesNotExist, Vacancy.DoesNotExist):
                raise Http404()
            return obj
        else:
            return None

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['obj'] = self.get_object()
        return ctx
