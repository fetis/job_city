# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-25 15:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('users', '0001_initial'),
        ('job_catalog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='vacancy',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.EmployerProfile', verbose_name='Профиль работодателя'),
        ),
        migrations.AddField(
            model_name='cv',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cat_cv', to='job_catalog.Category', verbose_name='Сфера деятельности'),
        ),
        migrations.AddField(
            model_name='cv',
            name='profile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.ApplicantProfile', verbose_name='Пользователь'),
        ),
    ]
