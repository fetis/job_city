from captcha.fields import ReCaptchaField
from django import forms
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError

from job_catalog.models import CV, Vacancy, Category, VacancyRequest
from users.forms import RegistrationApplicantForm, RegistrationEmployerForm
from users.models import User


class CVModelForm(forms.ModelForm):
    first_name = forms.CharField(label='Имя', max_length=255)
    last_name = forms.CharField(label='Фамилия', max_length=255)
    third_name = forms.CharField(label='Отчество', max_length=255)
    birth_day = forms.DateField(label='День рождения')
    phone = forms.CharField(label='Телефон', max_length=20)

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request
        if self.request.user.is_authenticated():
            self.profile = self.request.user.profile_applicant
            if self.profile:
                self.fields['first_name'].initial = self.profile.first_name
                self.fields['last_name'].initial = self.profile.last_name
                self.fields['third_name'].initial = self.profile.third_name
                self.fields['birth_day'].initial = self.profile.birth_day
                self.fields['phone'].initial = self.profile.phone
        else:
            self.profile = None

    def clean(self):
        if self.request.user.is_authenticated() and self.profile:
            if self.profile.cv_adverts.all().count() > 0:
                raise ValidationError("Нельзя создавать больше одного резюме, обратитесь в тех поддержку")
            return self.cleaned_data
        else:
            raise ValidationError("Вы не являетесь владельцем профиля, обратитесь в техподдержку")

    def save(self, commit=True):
        instance = super().save(commit=False)
        data = self.cleaned_data
        self.profile.first_name = data.get('first_name')
        self.profile.last_name = data.get('last_name')
        self.profile.third_name = data.get('third_name')
        self.profile.birth_day = data.get('birth_day')
        self.profile.phone = data.get('phone')
        self.profile.save()
        instance.profile = self.profile
        instance.save()
        return instance

    class Meta:
        model = CV
        fields = ['category', 'position', 'desired_salary', 'education', 'experience', 'skills', 'last_work']


class VacancyModelForm(forms.ModelForm):
    phone = forms.CharField(label='Телефон', max_length=20)
    company = forms.CharField(label='Название компании', max_length=255)
    contact = forms.CharField(label='Контактное лицо', max_length=255)
    address = forms.CharField(label='Адрес компании', max_length=300)

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = request

        if self.request.user.is_authenticated():
            self.profile = self.request.user.profile_employer
            if self.profile:
                self.fields['phone'].initial = self.profile.phone
                self.fields['company'].initial = self.profile.company
                self.fields['contact'].initial = self.profile.contact
                self.fields['address'].initial = self.profile.address
        else:
            self.profile = None

    def clean(self):
        if self.request.user.is_authenticated() and self.profile:
            return self.cleaned_data
        else:
            raise ValidationError("Вы не являетесь владельцем профиля, обратитесь в техподдержку")

    def save(self, commit=True):
        instance = super().save(commit=False)
        data = self.cleaned_data
        self.profile.phone = data.get('phone')
        self.profile.company = data.get('company')
        self.profile.contact = data.get('contact')
        self.profile.address = data.get('address')
        self.profile.save()
        instance.profile = self.profile
        instance.save()
        return instance

    class Meta:
        model = Vacancy
        fields = ['category', 'title', 'experience', 'charge', 'description', 'requirements', 'conditions',
                  'salary_min', 'salary_max']


class CVModelAndRegistrationForm(RegistrationApplicantForm):
    category = forms.ModelChoiceField(label='Сфера деятельности', queryset=Category.objects.all())
    position = forms.CharField(label='Предполагаемая должность', max_length=255)
    desired_salary = forms.IntegerField(label='Желаемая зарплата')
    education = forms.CharField(label='Образование', max_length=300, required=False)
    experience = forms.CharField(label='Опыт работы', required=False, widget=forms.Textarea)
    skills = forms.CharField(label='Дополнительные навыки', widget=forms.Textarea, required=False)
    last_work = forms.CharField(label='Последнее место работы', widget=forms.Textarea, required=False)
    captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['phone'].requred = True
        self.fields['first_name'].requred = True
        self.fields['last_name'].requred = True
        self.fields['third_name'].requred = True
        self.fields['birth_day'].requred = True

    class Meta(RegistrationApplicantForm.Meta):
        pass


class VacancyAndRegistrationForm(RegistrationEmployerForm):
    title = forms.CharField(max_length=100, label='Заголовок')
    experience = forms.CharField(max_length=50, label='Опыт работы', required=False)
    charge = forms.CharField(label='Обязанности', required=False, widget=forms.Textarea)
    description = forms.CharField(label='Описание вакансии', widget=forms.Textarea)
    requirements = forms.CharField(label='Требования', widget=forms.Textarea, required=False)
    conditions = forms.CharField(label='Условия', required=False, widget=forms.Textarea)
    salary_min = forms.IntegerField(label='Зарплата от', required=False)
    salary_max = forms.IntegerField(label='Зарплата до', required=False)
    category = forms.ModelChoiceField(label='Сфера деятельности', queryset=Category.objects.all())
    captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['phone'].requred = True
        self.fields['company'].requred = True
        self.fields['contact'].requred = True
        self.fields['address'].requred = True

    class Meta(RegistrationEmployerForm.Meta):
        pass


class ReplyCVForm(forms.Form):
    text = forms.CharField(label='Текст приглашения', widget=forms.Textarea())


class VacancyRequestForm(forms.ModelForm):
    captcha = ReCaptchaField()

    class Meta:
        model = VacancyRequest
        fields = '__all__'
