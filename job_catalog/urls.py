from django.conf.urls import url

from job_catalog.forms import CVModelAndRegistrationForm
from job_catalog.models import CV, Vacancy
from job_catalog.views import IndexView, CreateAdvertRegistrationView, VacancyAndRegistrationForm, CreateCVView, \
    CreateVacancyView, MyCvList, MyVacancyList, CVEditView, VacancyEditView, GetCvInfo, GetVacancyInfo, ReplySuccess, \
    ReplyCv, ReplyVacancy, AddToFavorites, DeleteFromFavorites, MyFavorites, ToPrint
from yandex_job.views import YandexJobGate

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^create_cv_unregister/$', CreateAdvertRegistrationView.as_view(form_class=CVModelAndRegistrationForm,
                                                                         model=CV), name='cv_unregister'),
    url(r'^create_vacancy_unregister/$', CreateAdvertRegistrationView.as_view(form_class=VacancyAndRegistrationForm,
                                                                              model=Vacancy,
                                                                              template_name='create_vacancy.html'),
        name='vacancy_unregister'),
    url(r'^create_cv/$', CreateCVView.as_view(), name='cv_create'),
    url(r'^create_vacancy/$', CreateVacancyView.as_view(), name='vacancy_create'),
    url(r'^my_cv/$', MyCvList.as_view(), name='my_cv_list'),
    url(r'^my_vacancies/$', MyVacancyList.as_view(), name='my_vac_list'),
    url(r'^update_cv/(?P<pk>\d+)/$', CVEditView.as_view(), name='update_cv'),
    url(r'^update_vacancies/(?P<pk>\d+)/$', VacancyEditView.as_view(), name='update_vacancy'),
    url(r'^get_cv_info/(?P<pk>\d+)/$', GetCvInfo.as_view(), name='get_cv_info'),
    url(r'^get_vacancy_info/(?P<pk>\d+)/$', GetVacancyInfo.as_view(), name='get_vacancy_info'),
    url(r'^reply_cv/(?P<pk>\d+)/$', ReplyCv.as_view(), name='cv_reply'),
    url(r'^reply_vacancy/(?P<pk>\d+)/$', ReplyVacancy.as_view(), name='vac_reply'),
    url(r'^success_reply/$', ReplySuccess.as_view(), name='success_reply'),
    url(r'^add_to_fav/(?P<slug>\w+)/(?P<pk>\d+)/$', AddToFavorites.as_view(), name='add_fav'),
    url(r'^delete_to_fav/(?P<slug>\w+)/(?P<pk>\d+)/$', DeleteFromFavorites.as_view(), name='delete_fav'),
    url(r'^my_favorites/$', MyFavorites.as_view(), name='my_fav'),
    url(r'^to_print/(?P<slug>\w+)/(?P<pk>\d+)/$', ToPrint.as_view(), name='to_print'),
    url(r'^vacancy_gate/$', YandexJobGate.as_view()),
]