# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-28 04:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0020_event'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newspaper',
            name='paper_file',
            field=models.FileField(upload_to='news_paper_files', verbose_name='Файл газеты'),
        ),
    ]
