# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-03 16:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0016_expertnote'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='slug',
            field=models.SlugField(max_length=100, unique=True, verbose_name='Машинное название'),
        ),
        migrations.AlterField(
            model_name='expertnote',
            name='slug',
            field=models.SlugField(max_length=100, unique=True, verbose_name='Машинное название'),
        ),
        migrations.AlterField(
            model_name='news',
            name='slug',
            field=models.SlugField(max_length=100, unique=True, verbose_name='Машинное название'),
        ),
    ]
