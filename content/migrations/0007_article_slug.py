# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-07 16:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content', '0006_auto_20180207_1615'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='slug',
            field=models.CharField(default='slug', max_length=100, verbose_name='Машинное название'),
            preserve_default=False,
        ),
    ]
