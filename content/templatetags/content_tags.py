from django import template

from content.models import NewsPaper, QAJurist, News, Article, ExpertNote

register = template.Library()


@register.assignment_tag
def get_last_newspaper():
    last_news_paper = NewsPaper.objects.all().first()
    return last_news_paper if last_news_paper else None


@register.simple_tag
def get_expert_counter():
    counter = ExpertNote.objects.all().count()
    return counter


@register.simple_tag
def get_news_counter():
    return News.objects.filter(published=True).count()


@register.simple_tag
def get_article_counter():
    return Article.objects.filter(published=True).count()