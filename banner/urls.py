from django.conf.urls import url

from banner.views import ClickOrRedirect

urlpatterns = [
    url(r'^(?P<pk>\d+)$', ClickOrRedirect.as_view(), name='banner_click'),
]