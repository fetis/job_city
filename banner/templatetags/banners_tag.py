from django import template
from django.conf import settings
from django.urls import reverse

from banner.models import Banner

register = template.Library()


@register.assignment_tag
def get_banners(path=None):
    banners = Banner.objects.get_banners()
    if banners:
        if path:
            path_banner = banners.get(path)
            if path_banner:
                banners[path_banner['position']] = path_banner
            return banners
        return banners
    else:
        return {}


@register.filter
def render_banner(banner):
    if banner:
        if banner.get('redirect'):
            return '<a href="{}" target="_blank"><img src="{}{}" alt=""></a>'.format(
                reverse("banner:banner_click", kwargs={'pk': banner['pk']}), settings.MEDIA_URL, banner['banner'])
        else:
            return '<a href="{media}{img}" data-url={url} class="fancy_banner" data-fancybox>' \
                   '<img src="{media}{img}" alt=""></a>'.format(media=settings.MEDIA_URL, img=banner['banner'],
                                                                url=reverse("banner:banner_click",
                                                                            kwargs={'pk': banner['pk']}))
    else:
        return ''
