from django.db.models import F
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
from django.views import View

from banner.models import Banner


class ClickOrRedirect(View):

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        if pk and pk.isdigit():
            banner = Banner.objects.filter(pk=pk).first()
            if banner:
                banner.views = F('views') + 1
                banner.save(update_fields=['views'])
                if banner.redirect_url:
                    return HttpResponseRedirect(banner.redirect_url)
        return HttpResponse()

