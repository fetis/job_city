from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import Banner


class BannerAdmin(ModelAdmin):
    list_filter = ['position']
    readonly_fields = ['views']
    list_display = ['title', 'views', 'path']


admin.site.register(Banner, BannerAdmin)
