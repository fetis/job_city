from django.conf import settings
from django.core.cache import cache
from django.db import models


class BannerManager(models.Manager):

    def set_banners(self):
        result = dict()
        items = Banner.objects.all().values('position', 'banner_image', 'pk', 'path', 'redirect_url')
        for item in items:
            if item.get('path'):
                result[item['path']] = {'banner': item['banner_image'], 'pk': item['pk'],
                                        'redirect': item['redirect_url'], 'position': item['position']}
            else:
                result[item['position']] = {'banner': item['banner_image'], 'pk': item['pk'],
                                            'redirect': item['redirect_url']}
        cache.set(settings.BANNERS_KEY, result, 20)
        return result

    def get_banners(self):
        result = cache.get(settings.BANNERS_KEY, None)
        if result is None:
            return self.set_banners()
        else:
            return result


class Banner(models.Model):
    title = models.CharField(verbose_name='Название', max_length=255)
    banner_image = models.ImageField(verbose_name='Баннер', upload_to='banners')
    position = models.IntegerField(verbose_name='Позиция баннера', choices=settings.POSITIONS_BANNER)
    path = models.CharField(verbose_name='Путь', help_text='Для отображения баннера на конкретной странице '
                                                           'пример /profile/', null=True, blank=True, max_length=255)
    views = models.PositiveIntegerField(default=0, verbose_name='Клики/Переходы')
    redirect_url = models.URLField(verbose_name='URL для редиректа', null=True, blank=True)

    @property
    def is_redirect(self):
        return bool(self.redirect_url)

    objects = BannerManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Баннер'
        verbose_name_plural = 'Баннеры'
