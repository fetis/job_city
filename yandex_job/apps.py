from django.apps import AppConfig


class YandexJobConfig(AppConfig):
    name = 'yandex_job'
